# Járművek detektálása konvolúciós neurális hálózat használatával
 
## Fejlesztési folyamat

- [x] Járművek felismerésére szolgáli konvolúciós neurális háló elkészítése
- [x] CNN finomhangolása
- [ ] Automatikus járműdetektálás (Első verzió, túl bonyolult (Végtelen szög, méret))
	- [x] Képpiramis implementálása
	- [X] Csúszóablakos detektálás
	- [ ] Optimalizálás
- [x] Járművek detektálása egyszeri felhasználói konfigurálással
	- [x] Grafikus felület létrehozása
	- [x] Megadható, hogy melyik irányba, hol detektáljon
	- [x] Json output generálása a detektálásról a Szimulációs komponensnek

