def sliding_window(img, offset, win_size):
    for y in range(0, img.shape[0] - win_size[1], offset):
        for x in range(0, img.shape[1] - win_size[0], offset):
            yield (x, y, img[y:y + win_size[1], x:x + win_size[0]])


