import imutils

def image_pyramid(img, scale, min_size=(200, 200)):
    yield img

    while img.shape[0] >= min_size[1] and img.shape[1] >= min_size[0]:
        w = int(img.shape[1] / scale)
        img = imutils.resize(img, width=w)

        yield img